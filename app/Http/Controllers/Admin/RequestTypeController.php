<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestTypeRequest;
use App\Models\RequestType;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class RequestTypeController extends Controller
{
    public function index(){
        if (request()->ajax()) {
            $types = RequestType::query()->get();
            return DataTables::make($types)
                ->escapeColumns([])
                ->addColumn('actions', function ($type) {
                    return $type->action_buttons;
                })
                ->rawColumns(['actions'])
                ->make();
        }
        return view('admins.requestType.index');
    }

    public function create(){
        return view('admins.requestType.create');
    }

    public function store(RequestTypeRequest $request){
        $data = $request->except('_token','has_image','is_enable');
        $data['has_image'] = $request->has('has_image') ? 1 : 0;
        $data['is_enable'] = $request->has('is_enable') ? 1 : 0;
        RequestType::query()->create($data);
        \LogActivity::addToLog('Create Request Type');
        toastr()->success('Create Successfully');
        return redirect()->route('admin.request.type.index');
    }

    public function edit($id){
        $type =RequestType::query()->find($id);
        return view('admins.requestType.edit',compact('type'));
    }

    public function update(RequestTypeRequest $request,$id){
        $type = RequestType::query()->find($id);
        $data = $request->except('_token','has_image','is_enable','id');
        $data['has_image'] = $request->has('has_image') ? 1 : 0;
        $data['is_enable'] = $request->has('is_enable') ? 1 : 0;
        $type->update($data);
        \LogActivity::addToLog('Update Request Type');
        toastr()->success('Updated Successfully');
        return redirect()->route('admin.request.type.index');
    }

    public function destroy(Request $request)
    {
        $file = RequestType::query()->find($request->id);
        $file->delete();
        \LogActivity::addToLog('Delete Request Type');
        return response()->json(['success' => true]);
    }

    public function change_state(Request $request){
        $category = RequestType::query()->find($request->id);
        $category->update([
            'is_enable'=> $category->is_enable == 1 ? 0 : 1
        ]);
        \LogActivity::addToLog('Change State Of Request Type');
        toastr()->success('Updated Successfully');
        return response()->json(['success' => true]);
    }


}
