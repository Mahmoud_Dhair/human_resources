<?php

namespace App\Http\Middleware;

use App\Http\Controllers\API\Auth\LoginController;
use App\Http\Controllers\Controller;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CheckAuth extends Controller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
//        $user = User::query()->where('email', $request->email)->first();
//        if (! $user || ! Hash::check($request->password, $user->password)) {
//            toastr()->error('Email Or Password May be Incorrect');
//            return redirect()->route('showLoginForm');
//        }
//        if($user->hasRole('admin')){
//            return $next($request);
//        }else{
//            toastr()->error('user Unauthorized');
//            return redirect()->route('showLoginForm');
//        }

        $user = User::query()->with('roles')->find(Auth::id());
        if(!$user->hasRole('Admin')){
            Auth::logout();
            $request->session()->invalidate();
            toastr()->error('user Unauthorized');
            return response()->view('admins.auth.login');
//            abort(403, 'user Unauthorized');
        }else{
            return $next($request);
        }

    }
}
