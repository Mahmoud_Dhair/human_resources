<?php

/**
 * Created By : Mahmoud Dhair,
 * Data : ,
 * Time : 2:49 PM,
 * Phone : 00970594751929
 */
namespace App\Helper;

use Illuminate\Support\Facades\Request;
use App\Models\LogActivity as LogActivityModel;

class LogActivity
{

    public static  function addToLog($subject){
        $log = [];
        $log['subject'] = $subject;
        $log['url'] = Request::fullUrl();
        $log['method'] = Request::method();
        $log['ip'] = Request::ip();
        $log['agent'] = Request::header('user-agent');
        $log['user_id'] = auth()->user()->name_en;
        LogActivityModel::query()->create($log);
    }

    public static function logActivityLists()
    {
        return LogActivityModel::latest()->get();
    }

}
