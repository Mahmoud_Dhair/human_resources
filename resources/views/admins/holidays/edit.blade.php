@extends('admins.layouts.layout')

@section('title') Edit Holiday @endsection

@section('style')

@endsection

@section('breadcrumb')
    <span class="kt-subheader__breadcrumbs-separator"></span>
    <a href="{{route('admin.holiday.index')}}" class="kt-subheader__breadcrumbs-link">Holiday</a>
    <span class="kt-subheader__breadcrumbs-separator"></span>
    <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Edit Holiday</span>
@endsection

@section('content')
    <div class="col-lg-12">

        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Edit Holiday
                    </h3>
                </div>
            </div>

            <!--begin::Form-->
            <form class="kt-form kt-form--label-right" method="POST"
                  action="{{route('admin.holiday.update',$holiday->id)}}"
                  enctype="multipart/form-data">
                @csrf
                <div class="kt-portlet__body">
                    <input type="hidden" name="id" value="{{$holiday->id}}">
                    <div class="form-group row">
                        <div class="offset-2"></div>
                        <div class="col-lg-6">
                            <label class="">Name :</label>
                            <input type="text" class="form-control" placeholder="Enter your Request Type" name="name"
                                   value="{{$holiday->name}}">
                            @error('name')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="offset-2"></div>
                        <div class="col-lg-6">
                            <label class="">Date :</label>
                            <input type="text" class="form-control" id="kt_datepicker_1"
                                   value="{{date('m/d/Y', strtotime($holiday->date))}}" name="date" autocomplete="false"
                                   placeholder="Select date"/>
                            @error('date')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="offset-2"></div>
                        <label class="col-1 col-form-label">Enable :</label>
                        <div class="col-2">
                                <span class="kt-switch kt-switch--icon">
                                    <label>
                                        <input type="checkbox" name="is_enable" @if ($holiday->is_enable == 1)
                                        checked
                                               @endif  value="1">
                                        <span></span>
                                    </label>
                                </span>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-lg-6">
                                <button type="submit" class="btn btn-primary">Create</button>
                                {{--                                <button type="reset" class="btn btn-secondary">Cancel</button>--}}
                            </div>
                            <div class="col-lg-6 kt-align-right">
                                <button type="reset" class="btn btn-danger">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

        <!--end::Portlet-->
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/js/demo1/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script>
    <!--begin::Page Scripts(used by this page) -->
    <script src="{{asset('assets/js/demo1/pages/crud/forms/widgets/bootstrap-switch.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/js/demo1/pages/crud/forms/widgets/bootstrap-datepicker.js')}}"
            type="text/javascript"></script>
@endsection
