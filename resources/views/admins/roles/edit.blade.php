@extends('admins.layouts.layout')

@section('title') Edit Role @endsection

@section('style')

@endsection

@section('breadcrumb')
    <span class="kt-subheader__breadcrumbs-separator"></span>
    <a href="{{route('user.role.index')}}" class="kt-subheader__breadcrumbs-link">Roles</a>
    <span class="kt-subheader__breadcrumbs-separator"></span>
    <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Edit Role</span>
@endsection

@section('content')
    <div class="col-md-12">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Edit Role
                    </h3>
                </div>
            </div>
            <!--begin::Form-->
            <form class="kt-form" method="POST" action="{{route('user.role.update',$role->id)}}">
                @csrf
                <div class="kt-portlet__body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label id="name">Name</label>
                            <input type="text" value="{{$role->name}}" class="form-control" id="name" name="name" placeholder="Role Name">
                            {{--                            <span class="form-text text-muted">We'll never share your email with anyone else.</span>--}}
                            @error('permission')
                            <p class="block-tag text-left">
                                <small class="badge badge-default badge-danger">{{$message}}</small>
                            </p>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Permissions</label>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <table class="table table-striped table-bordered w-100">
                                <tbody>
                                @foreach($permissions as $row)
                                    <tr class="">
                                        @foreach($row as $permission)
                                            <td>
                                                <input type="checkbox" name="permission[]" value="{{ $permission->id }}" {{in_array($permission->id,$rolePermissions) ? "checked" : false}}
                                                       class="">
                                                {{ $permission->name }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @error('permission')
                            <p class="block-tag text-left">
                                <small class="badge badge-default badge-danger">{{$message}}</small>
                            </p>
                            @enderror
                        </div>
                    </div>

                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

        <!--end::Portlet-->

    </div>

@endsection

@section('script')

@endsection
