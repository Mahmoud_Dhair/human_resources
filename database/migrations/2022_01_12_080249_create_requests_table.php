<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->id();
            $table->text('description');
            $table->dateTime('start')->nullable();
            $table->dateTime('end')->nullable();
            $table->unsignedBigInteger('request_type_id');
            $table->unsignedBigInteger('requester_id');
            $table->tinyInteger('status')->default(0)->comment('0 => Pending , 1 => Approval , 2 => Reject');
            $table->text('reply_message')->nullable();
            $table->timestamps();
            $table->foreign('request_type_id')->references('id')->on('request_types')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('requester_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
