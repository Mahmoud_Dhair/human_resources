<!DOCTYPE html>
<html lang="en" direction="ltr">
<!-- begin::Head -->
<head>
    @include('admins.layouts.head')
</head>
<!-- End::Head -->

<!-- begin::Body -->
<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
<!-- begin:: Page -->

<!-- begin:: Header Mobile -->
@include('admins.layouts.mobile_header')

<!-- End:: Header Mobile -->

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <!-- begin:: Aside -->
        @include('admins.layouts.aside')
        <!-- End:: Aside -->

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <!-- begin:: Header -->
                @include('admins.layouts.header')
            <!-- End:: Header -->

            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
                <!-- begin:: Subheader -->
                @include('admins.layouts.sub_header')
                <!-- end:: Subheader -->

                <!-- begin:: Content -->
                <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
                    @yield('content')
                </div>
                <!-- End:: Content -->
            </div>

            <!-- begin:: Footer -->
            @include('admins.layouts.footer')
            <!-- End:: Footer -->

        </div>

    </div>
</div>
<!-- begin:: Additions -->
@include('admins.layouts.additions')
<!-- End:: Additions -->

<!-- begin:: Script -->
@include('admins.layouts.script')
<!-- End:: Script -->

<!-- End:: Page -->
</body>
<!-- End::Body -->
</html>
