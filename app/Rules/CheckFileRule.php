<?php

namespace App\Rules;

use App\Models\FileType;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class CheckFileRule implements Rule
{
    public function passes($attribute, $value)
    {
        dd($attribute,$value);
        $state = false;
        $file = FileType::query()->where('name',$attribute)->first();
        $files = User::query()->find(request()->get('id'))->files;
        foreach ($files as $file){
           if($file->name === $attribute){
               $state = true;
           }
        }
        return $file->is_required == 1 && $state;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        dd(234234);
        return 'The File :attribute is required';
    }
}
