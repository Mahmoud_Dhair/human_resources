<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class Admin extends Authenticatable
{
    use HasFactory, SoftDeletes, Notifiable, HasApiTokens, HasRoles;

    protected $table = 'admins';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'email', 'password', 'phone', 'image'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'password', 'remember_token'
    ];

    public function getActionButtonsAttribute()
    {
        $button = '';
        $button .= '<a href="' . route('admin.edit',$this->id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="la la-edit"></i></a>';
        $button .= '<button  title="Delete Admin" type="button" data-id="' . $this->id . '" data-name="' . $this->name . '" data-toggle="modal" data-target="#deleteModel" class="btn btn-sm btn-clean btn-icon btn-icon-md btn-delete delete-item"><i class="la la-trash"></i></button>';
        return $button;
    }
}
