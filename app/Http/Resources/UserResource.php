<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name_ar' => $this->name_ar,
            'name_en' => $this->name_en,
            'email' => $this->email,
            'gender' => $this->gender,
            'date_of_birth' => $this->date_of_birth,
            'marital_status' => $this->marital_status,
            'num_children' => $this->num_children,
            'address' => $this->address,
            'phone' => $this->phone,
            'secondary_phone' => $this->secondary_phone,
            'telephone' => $this->telephone,
            'university' => $this->university,
            'faculty' => $this->faculty,
            'specialization' => $this->specialization,
            'files' => FileResource::collection($this->files) ?? [],
            'roles' => RoleResource::collection($this->roles)
        ];
    }
}
