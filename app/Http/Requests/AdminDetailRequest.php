<?php

namespace App\Http\Requests;

use App\Models\FileType;
use App\Models\User;
use App\Rules\CheckFileRule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class AdminDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $roles = [
            'id' => 'required|exists:users,id',
            'name_ar' => 'required',
            'name_en' => 'required',
            'email' => 'required|email|unique:users,email,' . $this->id,
            'roles_name' => 'required',
            'gender' => 'required',
            'date_of_birth' => 'required|date',
            'marital_status' => 'required',
            'num_children' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'secondary_phone' => 'required',
            'telephone' => 'required',
            'university' => 'required',
            'faculty' => 'required',
            'specialization' => 'required',
        ];

        $roles = [];

        $request = request();
        $files = FileType::query()->where('is_enable', 1)->get();
        $user = User::find($request->id);
        $user_files = $user->files;
        $is_required = false;
        foreach ($files as $file) {
            if ($user_files->where('type', $file->name)->count() > 0) {
                $is_required = false;
            } else {
                if ($file->is_required){
                    $is_required = true;
                } else{
                    $is_required = false;
                }
            }
            $roles[$file->name] = $is_required ? 'required|mimes:doc,docx,pdf' : '';
        }

        return $roles;
    }

    // protected function failedValidation(Validator $validator)
    // {
    //    throw new HttpResponseException(response()->json([
    //        'success' => false,
    //        'code' => 405,
    //        'message' => implode("\n",$validator->messages()->all())
    //    ]));
    // }
}
