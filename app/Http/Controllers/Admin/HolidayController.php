<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\HolidayRequest;
use App\Models\Holiday;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class HolidayController extends Controller
{
    public function index(){
        if (request()->ajax()) {
            $holidays = Holiday::query()->get();
            return DataTables::make($holidays)
                ->escapeColumns([])
                ->addColumn('date', function ($holiday) {
                    return date('d/m/Y', strtotime($holiday->date));
                })
                ->addColumn('actions', function ($holiday) {
                    return $holiday->action_buttons;
                })
                ->rawColumns(['actions'])
                ->make();
        }
        return view('admins.holidays.index');
    }

    public function create(){
        return view('admins.holidays.create');
    }

    public function store(HolidayRequest $request){
        $data = $request->except('_token','is_enable','date');
        $data['is_enable'] = $request->has('is_enable') ? 1 : 0;
        $data['date'] = date('Y-m-d H:i:s', strtotime($request->date));
        Holiday::query()->create($data);
        \LogActivity::addToLog('Create Holiday');
        toastr()->success('Create Successfully');
        return redirect()->route('admin.holiday.index');
    }

    public function edit($id){
        $holiday =Holiday::query()->find($id);
        return view('admins.holidays.edit',compact('holiday'));
    }

    public function update(HolidayRequest $request,$id){
        $type = Holiday::query()->find($id);
        $data = $request->except('_token','is_enable','id','date');
        $data['date'] = date('Y-m-d H:i:s', strtotime($request->date));
        $data['is_enable'] = $request->has('is_enable') ? 1 : 0;
        $type->update($data);
        \LogActivity::addToLog('Update Holiday');
        toastr()->success('Updated Successfully');
        return redirect()->route('admin.holiday.index');
    }

    public function destroy(Request $request)
    {
        $file = Holiday::query()->find($request->id);
        $file->delete();
        \LogActivity::addToLog('Delete Holiday');
        return response()->json(['success' => true]);
    }

    public function change_state(Request $request){
        $category = Holiday::query()->find($request->id);
        $category->update([
            'is_enable'=> $category->is_enable == 1 ? 0 : 1
        ]);
        \LogActivity::addToLog('Change State of Holiday');
        toastr()->success('Updated Successfully');
        return response()->json(['success' => true]);
    }

}
