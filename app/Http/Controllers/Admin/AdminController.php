<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminDetailRequest;
use App\Http\Requests\AdminRequest;
use App\Mail\RegistrionMail;
use App\Models\Admin;
use App\Models\FileType;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\DataTables;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        if (request()->ajax()) {
            $type = request()->get('type');
            $search = request()->get('search');
            $admins = User::where('id', '!=', Auth::id())
                ->when('search',function ($query) use ($search){
                    $query->where('name_en','like','%'. $search .'%');
                })
                ->when('type',function ($query) use ($type){
                    if($type == 1){
                        $query->whereHas('roles', function ($q) {
                            $q->where('name','<>','Admin');
                        });
                    }elseif ($type == 0){
                        $query->whereHas('roles', function ($q) {
                            $q->where('name','Admin');
                        });
                    }elseif ($type == 2){
                        return;
//                        $query->whereHas('roles');
                    }
                })
                ->get();


            return DataTables::make($admins)
                ->escapeColumns([])
                ->addColumn('department', function ($admin) {
                    return " - ";
                })
                ->addColumn('roles', function ($admin) {
                    $adminRole = $admin->roles->pluck('name', 'name')->all();
                    $roles = '';
                    foreach ($adminRole as $role) {
                        $roles .= '<span class="kt-badge kt-badge--primary  kt-badge--inline kt-badge--pill">' . $role . '</span>';
                    }
                    return $roles;
                })
                ->addColumn('name', function ($admin) {
                    return $admin->name_en;
                })
                ->addColumn('actions', function ($admin) {
                    return $admin->action_buttons;
                })
                ->rawColumns(['actions','name','roles','department'])
                ->make();
        }
        return view('admins.admins.index');
    }

    public function create()
    {
        $roles = Role::query()->pluck('name', 'name')->all();
        return view('admins.admins.create', compact('roles'));
    }

    public function store(AdminRequest $request)
    {
        $data = $request->except('_token', 'roles_name');
        $password = Str::random(15);
        $data['password'] = Hash::make($password);
        $user = User::query()->create($data);
        $user->assignRole($request->input('roles_name'));
        Mail::to($request->email)->send(new RegistrionMail(['password' => $password]));
        \LogActivity::addToLog('Create Employee');
        toastr()->success('Created Successfully');
        return redirect()->route('admin.index');
    }


    public function edit($id)
    {
        $admin = User::query()->find($id);
        $roles = Role::query()->pluck('name', 'name')->all();
        $adminRole = $admin->roles->pluck('name', 'name')->all();
        return view('admins.admins.edit', compact('admin', 'roles', 'adminRole'));
    }

    public function detail($id)
    {
        $admin = User::query()->find($id);
        $roles = Role::query()->pluck('name', 'name')->all();
        $adminRole = $admin->roles->pluck('name', 'name')->all();
        $files = FileType::query()->where('is_enable',1)->get();
        $user_files = $admin->files;
        return view('admins.admins.details', compact('admin', 'roles', 'adminRole','files','user_files'));
    }

    public function edit_detail($id)
    {
        $admin = User::query()->find($id);
        $roles = Role::query()->pluck('name', 'name')->all();
        $adminRole = $admin->roles->pluck('name', 'name')->all();
        $files = FileType::query()->where('is_enable',1)->get();
        return view('admins.admins.edit_details', compact('admin', 'roles', 'adminRole','files'));
    }

    public function update(AdminRequest $request, $id)
    {
        $admin = User::query()->find($id);
        $data = $request->except('_token', 'roles_name');
        $admin->update($data);
        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $admin->assignRole($request->input('roles_name'));
        \LogActivity::addToLog('Update Employee');
        toastr()->success('Updated Successfully');
        return redirect()->route('admin.index');
    }

    public function update_detail(AdminDetailRequest $request, $id)
    {
        $files = FileType::query()->where('is_enable',1)->get();
        $admin = User::query()->with('files')->find($id);
        $data = $request->except('_token', 'roles_name');
        $user_files = $admin->files;
        foreach ($files as $file){
            if($request->hasFile($file->name) && $request->file($file->name) != null){
                if($user_files->where('type',$file->name)->count() > 0){
                   Storage::disk('public')->delete($user_files->where('type','cv')[0]->src);
                    $user_files->where('type','cv')[0]->delete();
                    $path = uploadImage($request->file($file->name),'files');
                    $admin->files()->create([
                        'src' => $path,
                        'type' => $file->name
                    ]);
                }else{
                    $path = uploadImage($request->file($file->name),'files');
                    $admin->files()->create([
                        'src' => $path,
                        'type' => $file->name
                    ]);
                }

            }
        }
        $admin->update($data);
        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $admin->assignRole($request->input('roles_name'));
        \LogActivity::addToLog('Update Employee Details');
        toastr()->success('Updated Successfully');
        return redirect()->route('admin.index');
    }

    public function destroy(Request $request)
    {
        $admin = User::query()->find($request->id);
        $admin->delete();
        DB::table('model_has_roles')->where('model_id', $request->id)->delete();
        \LogActivity::addToLog('Delete Employee');
        return response()->json(['success' => true]);
    }
}
