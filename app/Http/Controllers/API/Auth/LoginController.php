<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    const MESSAGE = 'Email Or Password May be Incorrect';
    public function login(Request $request){
        $validator = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required'
        ],[
            'email.required' => 'The Email Field Is Required',
            'email.email' => 'The Email Field Is Email',
            'password.required' => 'The Password Field Is Required',
        ]);

        if($validator->fails()){
            return $this->sendError(implode("\n",$validator->messages()->all()),405);
        }
        $user = User::query()->where('email', $request->email)->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            return $this->sendError(LoginController::MESSAGE,401);
        }

        $token = $user->createToken('web')->plainTextToken;
        return $this->sendResponse([
           'user' => new UserResource($user),
            'token' => $token
        ],'Login Successfully');

    }

    public function logout(Request $request){
        Auth::guard('user-api')->user()->currentAccessToken()->delete();
        return $this->sendResponse([],'Logout Successfully');
    }

    public function submitForgetPasswordForm(Request $request){

        $validator = Validator::make($request->all(),[
            'email' => 'required|email'
        ],[
            'email.required' => 'The Email Is Required',
            'email.email' => 'The Email Must Be Email Format',
        ]);

        if($validator->fails()){
            return $this->sendError(implode("\n",$validator->messages()->all()),405);
        }

        $token = Str::random(64);

        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

        Mail::send('email.forgetPassword', ['token' => $token], function($message) use($request){
            $message->to($request->email);
            $message->subject('Reset Password');
        });

    }
}
