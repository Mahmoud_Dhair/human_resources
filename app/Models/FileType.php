<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileType extends Model
{
    use HasFactory;
    protected $table = 'file_types';


    protected $fillable = [
        'name','is_enable','is_required'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $appends = ['validation'];

    /////////////////////////////////// Append /////////////////////////////////////////

    public function getValidationAttribute(){
        return $this->is_required == 1 ? "required|mimes:doc,docx,pdf" : "nullable|mimes:doc,docx,pdf";
    }

    /// /////////////////////////////////// Append /////////////////////////////////////////

    public function getActionButtonsAttribute()
    {
        $button = '';
        $button .= '<a href="' . route('admin.file.edit',$this->id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="la la-edit"></i></a>';
        $button .= '<button  title="Delete Admin" type="button" data-id="' . $this->id . '" data-name="' . $this->name . '" data-toggle="modal" data-target="#deleteModel" class="btn btn-sm btn-clean btn-icon btn-icon-md btn-delete delete-item"><i class="la la-trash"></i></button>';
        if($this->is_enable == 1){
            $button .= '<button  title="Change state" type="button" data-id="' . $this->id . '"  class="btn btn-sm btn-clean btn-icon btn-icon-md btn-delete delete-item status"><i class="la la-close text-danger"></i></button>';
        }else{
            $button .= '<button  title="Change state" type="button" data-id="' . $this->id . '"  class="btn btn-sm btn-clean btn-icon btn-icon-md btn-delete delete-item status"><i class="la la-check text-info"></i></button>';
        }
        return $button;
    }
}
