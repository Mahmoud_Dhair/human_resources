@extends('admins.layouts.layout')

@section('title') Users @endsection

@section('style')
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('breadcrumb')
    <span class="kt-subheader__breadcrumbs-separator"></span>
    <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Users</span>
@endsection

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">

            <div class="kt-portlet__head-label">

                <h3 class="kt-portlet__head-title">
                    Users Management
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
{{--                        <div class="dropdown dropdown-inline">--}}
{{--                            <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle"--}}
{{--                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                                <i class="la la-download"></i> Export--}}
{{--                            </button>--}}
{{--                            <div class="dropdown-menu dropdown-menu-right">--}}
{{--                                <ul class="kt-nav">--}}
{{--                                    <li class="kt-nav__section kt-nav__section--first">--}}
{{--                                        <span class="kt-nav__section-text">Choose an option</span>--}}
{{--                                    </li>--}}
{{--                                    <li class="kt-nav__item">--}}
{{--                                        <a href="#" class="kt-nav__link">--}}
{{--                                            <i class="kt-nav__link-icon la la-print"></i>--}}
{{--                                            <span class="kt-nav__link-text">Print</span>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                    <li class="kt-nav__item">--}}
{{--                                        <a href="#" class="kt-nav__link">--}}
{{--                                            <i class="kt-nav__link-icon la la-copy"></i>--}}
{{--                                            <span class="kt-nav__link-text">Copy</span>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                    <li class="kt-nav__item">--}}
{{--                                        <a href="#" class="kt-nav__link">--}}
{{--                                            <i class="kt-nav__link-icon la la-file-excel-o"></i>--}}
{{--                                            <span class="kt-nav__link-text">Excel</span>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                    <li class="kt-nav__item">--}}
{{--                                        <a href="#" class="kt-nav__link">--}}
{{--                                            <i class="kt-nav__link-icon la la-file-text-o"></i>--}}
{{--                                            <span class="kt-nav__link-text">CSV</span>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                    <li class="kt-nav__item">--}}
{{--                                        <a href="#" class="kt-nav__link">--}}
{{--                                            <i class="kt-nav__link-icon la la-file-pdf-o"></i>--}}
{{--                                            <span class="kt-nav__link-text">PDF</span>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        &nbsp;
                        <a href="{{route('admin.create')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            New User
                        </a>
                    </div>
                </div>
            </div>

        </div>
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <form id="filter">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" name="search" placeholder="Search..." id="search">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                </div>
                            </div>
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Type:</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <select class="form-control" name="type" id="type">
                                            <option value="2">All</option>
                                            <option value="0">Admin</option>
                                            <option value="1">Employee</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
{{--                                    <div class="kt-form__label">--}}
{{--                                        <label>Type:</label>--}}
{{--                                    </div>--}}
                                    <div class="kt-form__control">

                                        <button type="submit" class="form-control btn btn-info" id="kt_search">Search</button>
{{--                                        <select class="form-control bootstrap-select" id="kt_form_type">--}}
{{--                                            <option value="">All</option>--}}
{{--                                            <option value="1">Online</option>--}}
{{--                                            <option value="2">Retail</option>--}}
{{--                                            <option value="3">Direct</option>--}}
{{--                                        </select>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!--end: Search Form -->


            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable data-table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Department</th>
                    <th>actions</th>
                </tr>
                </thead>
            </table>

            <!--end: Datatable -->
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="deleteModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Delete Admin</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <h5>Are You Sour to Delete Admin</h5>
                    <form method="post" action="">
                        @csrf
                        <input type="hidden" id="id_item" name="id_item">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button"  class="delete btn btn-danger">Delete</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script src="https://cdn.datatables.net/1.11.0/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.0/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function () {

            $(document).on('click', '.delete-item', (function () {
                var id = $(this).data("id");
                $('.modal-body #id_item').val(id);
            }));


            $('.delete').click(function (e) {
                e.preventDefault();
                var id = $('#id_item').val();
                var token = $('#id_item').prev().val();
                var url = '{{route('admin.destroy')}}';
                var type = "post";
                $.ajax({
                    type: type,
                    url: url,
                    data: {
                        'id': id,
                        '_token': token
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.success === true) {
                            $('#deleteModel').css('display','none');
                            $('.modal-backdrop').css('display','none');
                            toastr.success('تم الحذف بنجاح');
                            $('.data-table').DataTable().ajax.reload();
                        }
                    },
                    error: function (data) {

                    }
                });
            });

            $(function () {
                var table = $('.data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ordering: true,
                    searching: false,
                    dom: 'lBfrtip',
                    // buttons: [
                    //     'excel'
                    // ],

{{--                    @if(app()->getLocale() == 'ar')--}}
{{--                    language: {--}}
{{--                        url: "http://cdn.datatables.net/plug-ins/1.10.21/i18n/Arabic.json"--}}
{{--                    },--}}
{{--                    @endif--}}
                    ajax: {
                        url: '{{ route('admin.index') }}',
                        data: function (d) {
                            d.type = $("#type").val();
                            d.search = $("#search").val();
                        }
                    },
                    columnDefs: [
                        {
                            "targets": 0, // your case first column
                            "className": "text-center",
                        },
                        {
                            "targets": 1, // your case first column
                            "className": "text-center",
                        },
                        {
                            "targets": 2, // your case first column
                            "className": "text-center",
                        },
                        {
                            "targets": 3, // your case first column
                            "className": "text-center",
                        },
                        {
                            "targets": 4, // your case first column
                            "className": "text-center",
                        },
                    ],
                    columns: [
                        {data: 'name_en', name: 'name_en'},
                        {data: 'email', name: 'email'},
                        {data: 'roles', name: 'roles'},
                        {data: 'department', name: 'department'},
                        {data: 'actions', name: 'actions'}
                    ],
                });

                $('#kt_search').click(function(e){
                    e.preventDefault();
                    console.log("test");
                    table.draw()
                    // $('#data-table').DataTable().draw(true);
                });
            });



        });
    </script>
@endsection
