@extends('admins.layouts.layout')

@section('title') Roles @endsection

@section('style')
    <link href="{{asset('assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('breadcrumb')
    <span class="kt-subheader__breadcrumbs-separator"></span>
    <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Role Management</span>
@endsection

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    Role Management
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        {{--                                                <div class="dropdown dropdown-inline">--}}
                        {{--                                                    <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                        {{--                                                        <i class="la la-download"></i> Export--}}
                        {{--                                                    </button>--}}
                        {{--                                                    <div class="dropdown-menu dropdown-menu-right">--}}
                        {{--                                                        <ul class="kt-nav">--}}
                        {{--                                                            <li class="kt-nav__section kt-nav__section--first">--}}
                        {{--                                                                <span class="kt-nav__section-text">Choose an option</span>--}}
                        {{--                                                            </li>--}}
                        {{--                                                            <li class="kt-nav__item">--}}
                        {{--                                                                <a href="#" class="kt-nav__link">--}}
                        {{--                                                                    <i class="kt-nav__link-icon la la-print"></i>--}}
                        {{--                                                                    <span class="kt-nav__link-text">Print</span>--}}
                        {{--                                                                </a>--}}
                        {{--                                                            </li>--}}
                        {{--                                                            <li class="kt-nav__item">--}}
                        {{--                                                                <a href="#" class="kt-nav__link">--}}
                        {{--                                                                    <i class="kt-nav__link-icon la la-copy"></i>--}}
                        {{--                                                                    <span class="kt-nav__link-text">Copy</span>--}}
                        {{--                                                                </a>--}}
                        {{--                                                            </li>--}}
                        {{--                                                            <li class="kt-nav__item">--}}
                        {{--                                                                <a href="#" class="kt-nav__link">--}}
                        {{--                                                                    <i class="kt-nav__link-icon la la-file-excel-o"></i>--}}
                        {{--                                                                    <span class="kt-nav__link-text">Excel</span>--}}
                        {{--                                                                </a>--}}
                        {{--                                                            </li>--}}
                        {{--                                                            <li class="kt-nav__item">--}}
                        {{--                                                                <a href="#" class="kt-nav__link">--}}
                        {{--                                                                    <i class="kt-nav__link-icon la la-file-text-o"></i>--}}
                        {{--                                                                    <span class="kt-nav__link-text">CSV</span>--}}
                        {{--                                                                </a>--}}
                        {{--                                                            </li>--}}
                        {{--                                                            <li class="kt-nav__item">--}}
                        {{--                                                                <a href="#" class="kt-nav__link">--}}
                        {{--                                                                    <i class="kt-nav__link-icon la la-file-pdf-o"></i>--}}
                        {{--                                                                    <span class="kt-nav__link-text">PDF</span>--}}
                        {{--                                                                </a>--}}
                        {{--                                                            </li>--}}
                        {{--                                                        </ul>--}}
                        {{--                                                    </div>--}}
                        {{--                                                </div>--}}
                        &nbsp;
                        <a href="{{route('user.role.create')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            New Role
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0 ?>
                @foreach ($roles as $role)
                    <tr>
                        <?php $i++ ?>
                        <td>{{$i}}</td>
                        <td>{{$role->name}}</td>
                        <td>
                            <a href="{{route('user.role.edit',$role->id)}}"
                               class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>
                            <button class="btn btn-sm btn-clean btn-icon btn-icon-md btn-delete" data-id="{{$role->id}}"
                                    data-name="{{$role->name}}" data-toggle="modal" data-target="#delete">
                                <i class=" la la-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>

    <div class="modal fade text-left" id="delete" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel10"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger white">
                    <h4 class="modal-title white"
                        id="myModalLabel10">Delete Role</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('user.role.destroy')}}" method="post">
                    @csrf
                    <div class="modal-body">
                        <h4>Are You Sure To Delete Role </h4><br>
                        <input type="hidden" name="role_id" id="role_id" value="">
                        <input type="text" class="form-control" name="role_name" id="role_name"
                               readonly>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">
                            cancel
                        </button>
                        <button type="submit"
                                class="btn btn-outline-danger">Delete
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <!-- Bootstrap JavaScript -->
    {{--    <!--begin::Page Vendors(used by this page) -->--}}
    {{--    <script src="{{asset('assets/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>--}}

    {{--    <!--begin::Page Scripts(used by this page) -->--}}
    {{--    <script src="{{asset('assets/js/demo1/pages/crud/datatables/data-sources/html.js')}}"--}}
    {{--            type="text/javascript"></script>--}}

    <script>


        $(function () {
            $('#delete').on('show.bs.modal', function (event) {
                let button = $(event.relatedTarget);
                console.log(button.data('name'));
                let user_id = button.data('id');
                let user_name = button.data('name');
                var model = $(this);
                model.find('.modal-body #role_id').val(user_id);
                model.find('.modal-body #role_name').val(user_name);
            })

        });

    </script>
@endsection
