<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\LogActivity;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class LogActivityController extends Controller
{
    public function index(){
        if(request()->ajax()){
            $logs = \LogActivity::logActivityLists();
            return DataTables::make($logs)
                ->escapeColumns([])
                ->addIndexColumn()
                ->addColumn('method', function ($log) {
                    return '<span class="kt-badge kt-badge--primary  kt-badge--inline kt-badge--pill">' . $log->method . '</span>';
                })
                ->addColumn('created_at', function ($log) {
                    return date('d/m/Y', strtotime($log->created_at));
                })
                ->addColumn('actions', function ($log) {
                    return $log->action_buttons;
                })
                ->rawColumns(['actions','method','created_at'])
                ->make();
        }
        return view('admins.logs.index');
    }

    public function destroy(Request $request)
    {
        $file = LogActivity::query()->find($request->id);
        $file->delete();
        return response()->json(['success' => true]);
    }
}
