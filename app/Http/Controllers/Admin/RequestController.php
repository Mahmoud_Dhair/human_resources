<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class RequestController extends Controller
{
    public function index(){
        if (request()->ajax()) {
            $requests = \App\Models\Request::query()->get();
            return DataTables::make($requests)
                ->escapeColumns([])
                ->addColumn('name', function ($request) {
                    return $request->user->name_en;
                })
                ->addColumn('request_type', function ($request) {
                    return $request->request_type->name;
                })
                ->addColumn('status', function ($request) {
                    return $request->status_html;
                })
                ->addColumn('actions', function ($request) {
                    return $request->action_buttons;
                })
                ->rawColumns(['actions','status','request_type','name'])
                ->make();
        }
        return view('admins.requests.index');
    }
}
