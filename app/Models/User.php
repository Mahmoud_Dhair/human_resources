<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable,SoftDeletes,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name_ar',
        'name_en',
        'email',
        'password',
        'gender',
        'date_of_birth',
        'marital_status',
        'num_children',
        'address',
        'phone',
        'secondary_phone',
        'telephone',
        'university',
        'faculty',
        'specialization',
    ];
    protected $dates = ['deleted_at'];

//    protected $appends = ['type'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /////////////////////////////////////////////// Relation //////////

    public function files(){
        return $this->morphMany(File::class,'fileable');
    }

    /////////////////////////////////////////////// Relation //////////

    public function getActionButtonsAttribute()
    {
        $button = '';
        $button .= '<a href="' . route('admin.detail',$this->id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="la la-eye"></i></a>';
        $button .= '<a href="' . route('admin.edit',$this->id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="la la-edit"></i></a>';
        $button .= '<button  title="Delete Admin" type="button" data-id="' . $this->id . '" data-name="' . $this->name . '" data-toggle="modal" data-target="#deleteModel" class="btn btn-sm btn-clean btn-icon btn-icon-md btn-delete delete-item"><i class="la la-trash"></i></button>';
        return $button;
    }




}
