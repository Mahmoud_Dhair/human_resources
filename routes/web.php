<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\FileTypeController;
use App\Http\Controllers\Admin\HolidayController;
use App\Http\Controllers\Admin\LogActivityController;
use App\Http\Controllers\Admin\RequestController;
use App\Http\Controllers\Admin\RequestTypeController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\HomeController;
use App\Models\FileType;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test',function (){
//    $details = [
//        'title' => 'Mail from ItSolutionStuff.com',
//        'body' => 'This is for testing email using smtp'
//    ];
//    Mail::to('mahmmoddhair@gmail.com')->send(new \App\Mail\MyTestMail($details));
//    dd("Email is Sent.");

    $state = false;
    $file = FileType::query()->where('name','cv')->first();
    $files = User::query()->find(1)->files;
    foreach ($files as $file){
        if($file->name === 'cv'){
            $state = true;
        }
    }
    if($file->is_required == 1 && $state){
        return 1;
    }else {
        return 0;
    }
});

Route::group(['middleware' => 'guest'],function (){
    Route::get('/',[LoginController::class,'showLoginForm'])->name('showLoginForm');
    Route::post('/login',[LoginController::class,'login'])->name('login');
});

Route::group(['middleware' => ['auth','check_auth']],function (){
    Route::get('/dashboard',[HomeController::class,'index'])->name('user.index');
    Route::post('/logout',[LoginController::class,'logout'])->name('logout');


    Route::group(['prefix' => 'admin/roles'],function (){
        Route::get('roles',[RoleController::class,'index'])->name('user.role.index');
        Route::get('create',[RoleController::class,'create'])->name('user.role.create');
        Route::post('store',[RoleController::class,'store'])->name('user.role.store');
        Route::get('edit/{id}',[RoleController::class,'edit'])->name('user.role.edit');
        Route::post('update/{id}',[RoleController::class,'update'])->name('user.role.update');
        Route::post('destroy',[RoleController::class,'destroy'])->name('user.role.destroy');
    });

    Route::group(['prefix' => 'admin'],function (){
        Route::get('/',[AdminController::class,'index'])->name('admin.index');
        Route::get('create',[AdminController::class,'create'])->name('admin.create');
        Route::post('store',[AdminController::class,'store'])->name('admin.store');
        Route::get('edit/{id}',[AdminController::class,'edit'])->name('admin.edit');
        Route::get('detail/{id}',[AdminController::class,'detail'])->name('admin.detail');
        Route::get('edit-detail/{id}',[AdminController::class,'edit_detail'])->name('admin.edit.detail');
        Route::post('update-detail/{id}',[AdminController::class,'update_detail'])->name('admin.update.detail');
        Route::post('update/{id}',[AdminController::class,'update'])->name('admin.update');
        Route::post('destroy',[AdminController::class,'destroy'])->name('admin.destroy');
    });
    Route::group(['prefix' => 'file'],function (){
        Route::get('/',[FileTypeController::class,'index'])->name('admin.file.index');
        Route::get('create',[FileTypeController::class,'create'])->name('admin.file.create');
        Route::post('store',[FileTypeController::class,'store'])->name('admin.file.store');
        Route::get('edit/{id}',[FileTypeController::class,'edit'])->name('admin.file.edit');
        Route::get('change-state/{id}',[FileTypeController::class,'change_state'])->name('admin.file.change.state');
        Route::post('update/{id}',[FileTypeController::class,'update'])->name('admin.file.update');
        Route::post('destroy',[FileTypeController::class,'destroy'])->name('admin.file.destroy');
    });

    Route::group(['prefix' => 'request-type'],function (){
        Route::get('/',[RequestTypeController::class,'index'])->name('admin.request.type.index');
        Route::get('create',[RequestTypeController::class,'create'])->name('admin.request.type.create');
        Route::post('store',[RequestTypeController::class,'store'])->name('admin.request.type.store');
        Route::get('edit/{id}',[RequestTypeController::class,'edit'])->name('admin.request.type.edit');
        Route::get('change-state/{id}',[RequestTypeController::class,'change_state'])->name('admin.request.type.change.state');
        Route::post('update/{id}',[RequestTypeController::class,'update'])->name('admin.request.type.update');
        Route::post('destroy',[RequestTypeController::class,'destroy'])->name('admin.request.type.destroy');
    });

    Route::group(['prefix' => 'request'],function (){
        Route::get('/',[RequestController::class,'index'])->name('admin.request.index');
        Route::get('create',[RequestController::class,'create'])->name('admin.request.create');
        Route::post('store',[RequestController::class,'store'])->name('admin.request.store');
        Route::get('edit/{id}',[RequestController::class,'edit'])->name('admin.request.edit');
        Route::get('change-state/{id}',[RequestController::class,'change_state'])->name('admin.request.change.state');
        Route::post('update/{id}',[RequestController::class,'update'])->name('admin.request.update');
        Route::post('destroy',[RequestController::class,'destroy'])->name('admin.request.destroy');
    });

    Route::group(['prefix' => 'holiday'],function (){
        Route::get('/',[HolidayController::class,'index'])->name('admin.holiday.index');
        Route::get('create',[HolidayController::class,'create'])->name('admin.holiday.create');
        Route::post('store',[HolidayController::class,'store'])->name('admin.holiday.store');
        Route::get('edit/{id}',[HolidayController::class,'edit'])->name('admin.holiday.edit');
        Route::get('change-state/{id}',[HolidayController::class,'change_state'])->name('admin.holiday.change.state');
        Route::post('update/{id}',[HolidayController::class,'update'])->name('admin.holiday.update');
        Route::post('destroy',[HolidayController::class,'destroy'])->name('admin.holiday.destroy');
    });

    Route::group(['prefix' => 'logs'],function (){
        Route::get('/',[LogActivityController::class,'index'])->name('admin.logs.index');
        Route::post('destroy',[LogActivityController::class,'destroy'])->name('admin.logs.destroy');
    });
});

//Route::get('/', function () {
//    return view('admins.auth.login');
//});



//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();
