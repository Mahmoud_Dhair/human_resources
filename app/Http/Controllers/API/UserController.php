<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function profile(){
       $id  = Auth::guard('user-api')->id();
       $user = User::query()->find($id);
       return $this->sendResponse(new UserResource($user),'');
    }

    public function edit(){
        $id  = Auth::guard('user-api')->id();
        $user = User::query()->find($id);
        
    }


}
