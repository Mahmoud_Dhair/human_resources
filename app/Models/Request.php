<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    use HasFactory;

    protected $table = 'requests';

    protected $fillable = [
        'description','start','end','request_type_id','requester_id','status','reply_message'
    ];

    ////////////////////////////////////// Relation ////////////////////////////////

    public function request_type(){
        return $this->belongsTo(RequestType::class,'request_type_id','id');
    }

    public function user(){
        return $this->belongsTo(User::class,'requester_id','id');
    }

    ///////////////////////////////////////// Datatable //////////////////////////

    public function getActionButtonsAttribute()
    {
        $button = '';
        $button .= '<a href="' . route('admin.detail',$this->id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="la la-eye"></i></a>';
//        $button .= '<a href="' . route('admin.edit',$this->id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="la la-edit"></i></a>';
//        $button .= '<button  title="Delete Admin" type="button" data-id="' . $this->id . '" data-name="' . $this->name . '" data-toggle="modal" data-target="#deleteModel" class="btn btn-sm btn-clean btn-icon btn-icon-md btn-delete delete-item"><i class="la la-trash"></i></button>';
        return $button;
    }

    public function getStatusHtmlAttribute()
    {
        if($this->status == 0){
            return '<span class="kt-badge kt-badge--primary  kt-badge--inline kt-badge--pill">Pending</span>';
        }elseif ($this->status == 1){
            return '<span class="kt-badge kt-badge--success  kt-badge--inline kt-badge--pill">Approval</span>';
        }else{
            return '<span class="kt-badge kt-badge--danger  kt-badge--inline kt-badge--pill">Reject</span>';
        }
    }
}
