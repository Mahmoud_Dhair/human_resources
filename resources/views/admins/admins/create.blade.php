@extends('admins.layouts.layout')

@section('title') Create User @endsection

@section('style')

@endsection

@section('breadcrumb')
    <span class="kt-subheader__breadcrumbs-separator"></span>
    <a href="{{route('admin.index')}}" class="kt-subheader__breadcrumbs-link">Users</a>
    <span class="kt-subheader__breadcrumbs-separator"></span>
    <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Create User</span>
@endsection

@section('content')
    <div class="col-lg-12">

        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Create User
                    </h3>
                </div>
            </div>

            <!--begin::Form-->
            <form class="kt-form kt-form--label-right" method="POST" action="{{route('admin.store')}}" enctype="multipart/form-data">
                @csrf
                <div class="kt-portlet__body">

                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label class="">Email :</label>
                            <input type="email" class="form-control" placeholder="Enter your Email" name="email"
                                   value="{{old('email')}}">
                            @error('email')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-lg-6">
                            <label>Roles :</label>
                            <select class="form-control kt-select2" id="kt_select2_3" name="roles_name[]" multiple="multiple">
                                @foreach ($roles as $role)
                                    <option value="{{$role}}">{{$role}}</option>
                                @endforeach
                            </select>
                            @error('roles_name')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>
{{--                        <div class="col-lg-6">--}}
{{--                            <label>Name :</label>--}}
{{--                            <input type="text" class="form-control" placeholder="Enter your Name" name="name"--}}
{{--                                   value="{{old('name')}}">--}}
{{--                            @error('name')--}}
{{--                            <span class="form-text text-danger">{{$message}}</span>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
                    </div>
{{--                    <div class="form-group row">--}}
{{--                        <div class="col-lg-6">--}}
{{--                            <label>Password :</label>--}}
{{--                            <div class="kt-input-icon">--}}
{{--                                <input type="password" class="form-control" placeholder="Enter your Password" name="password">--}}
{{--                                <span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i--}}
{{--                                            class="la la-key"></i></span></span>--}}
{{--                            </div>--}}
{{--                            @error('password')--}}
{{--                            <span class="form-text text-danger">{{$message}}</span>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
{{--                        <div class="col-lg-6">--}}
{{--                            <label class="">Phone :</label>--}}
{{--                            <div class="kt-input-icon">--}}
{{--                                <input type="number" class="form-control" placeholder="Enter your Phone" name="phone">--}}
{{--                                <span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i--}}
{{--                                            class="la la-mobile-phone"></i></span></span>--}}
{{--                            </div>--}}
{{--                            @error('phone')--}}
{{--                            <span class="form-text text-danger">{{$message}}</span>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="form-group row">--}}
{{--                        <div class="col-lg-6">--}}
{{--                            <label>Roles :</label>--}}
{{--                            <select class="form-control kt-select2" id="kt_select2_3" name="roles_name[]" multiple="multiple">--}}
{{--                                @foreach ($roles as $role)--}}
{{--                                    <option value="{{$role}}">{{$role}}</option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
{{--                            @error('roles_name')--}}
{{--                            <span class="form-text text-danger">{{$message}}</span>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
{{--                        <div class="col-lg-6">--}}
{{--                            <label>Upload Image</label>--}}
{{--                            <div></div>--}}
{{--                            <div class="custom-file">--}}
{{--                                <input type="file" class="custom-file-input" name="image" id="customFile">--}}
{{--                                <label class="custom-file-label" for="customFile">Choose Image</label>--}}
{{--                            </div>--}}
{{--                            @error('image')--}}
{{--                            <span class="form-text text-danger">{{$message}}</span>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-lg-6">
                                <button type="submit" class="btn btn-primary">Create</button>
{{--                                <button type="reset" class="btn btn-secondary">Cancel</button>--}}
                            </div>
                            <div class="col-lg-6 kt-align-right">
                                <button type="reset" class="btn btn-danger">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

        <!--end::Portlet-->
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/js/demo1/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script>
@endsection
