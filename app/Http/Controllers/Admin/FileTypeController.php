<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\FileTypeRequest;
use App\Models\FileType;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class FileTypeController extends Controller
{
    public function index(){
        if(request()->ajax()){
            $files = FileType::query()->get();
            return DataTables::make($files)
                ->escapeColumns([])
                ->addColumn('actions', function ($file) {
                    return $file->action_buttons;
                })
                ->rawColumns(['actions'])
                ->make();
        }
        return view('admins.filesTypes.index');
    }

    public function create(){
        return view('admins.filesTypes.create');
    }

    public function store(FileTypeRequest $request){
        $data = $request->except('_token','is_enable','is_required');
        $data['is_enable'] = $request->has('is_enable') ? 1 : 0;
        $data['is_required'] = $request->has('is_required') ? 1 : 0;
        FileType::query()->create($data);
        \LogActivity::addToLog('Create File Type');
        toastr()->success('Created Successfully');
        return redirect()->route('admin.file.index');
    }

    public function edit($id){
        $file = FileType::query()->find($id);
        return view('admins.filesTypes.edit',compact('file'));
    }

    public function update(FileTypeRequest $request,$id){
        $file = FileType::query()->find($id);
        $data = $request->except('_token','is_enable','is_required','id');
        $data['is_enable'] = $request->has('is_enable') ? 1 : 0;
        $data['is_required'] = $request->has('is_required') ? 1 : 0;
        $file->update($data);
        \LogActivity::addToLog('Update File Type');
        toastr()->success('Updated Successfully');
        return redirect()->route('admin.file.index');
    }

    public function destroy(Request $request)
    {
        $file = FileType::query()->find($request->id);
        $file->delete();
        \LogActivity::addToLog('Delete File Type');
        return response()->json(['success' => true]);
    }

    public function change_state(Request $request){
        $category = FileType::query()->find($request->id);
        $category->update([
            'is_enable'=> $category->is_enable == 1 ? 0 : 1
        ]);
        \LogActivity::addToLog('Change State Of File Type');
        toastr()->success('Updated Successfully');
        return response()->json(['success' => true]);
    }
}
