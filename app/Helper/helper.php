<?php


use Illuminate\Support\Facades\Storage;

function uploadImage($image, $directory)
{
    $image_name = time() + rand(1, 1000000000) . '.' . $image->getClientOriginalExtension();
    $path = 'uploads/images/' . $directory . '/' . $image_name;
    Storage::disk('public')->put($path, file_get_contents($image));
    return $path;
}
