@extends('admins.layouts.layout')

@section('title')Edit User Detail @endsection

@section('style')

@endsection

@section('breadcrumb')
    <span class="kt-subheader__breadcrumbs-separator"></span>
    <a href="{{route('admin.index')}}" class="kt-subheader__breadcrumbs-link">Users</a>
    <span class="kt-subheader__breadcrumbs-separator"></span>
    <a href="{{route('admin.detail',$admin->id)}}" class="kt-subheader__breadcrumbs-link">User Details</a>
    <span class="kt-subheader__breadcrumbs-separator"></span>
    <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Edit User Detail</span>
@endsection

@section('content')
    <div class="col-lg-12">

        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Edit User Detail
                    </h3>
                </div>
            </div>

            <!--begin::Form-->
            <form class="kt-form kt-form--label-right" method="POST"
                  action="{{route('admin.update.detail',$admin->id)}}"
                  enctype="multipart/form-data">
                @csrf
                <div class="kt-portlet__body">
                    <input type="hidden" name="id" value="{{$admin->id}}">

                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Name In Arabic :</label>
                            <input type="text" class="form-control" placeholder="Enter your Name" name="name_ar"
                                   value="{{$admin->name_ar}}">
                            @error('name')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="col-lg-6">
                            <label>Name In English :</label>
                            <input type="text" class="form-control" placeholder="Enter your Name" name="name_en"
                                   value="{{$admin->name_en}}">
                            @error('name')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="col-lg-6">
                            <label class="">Email :</label>
                            <input type="email" class="form-control" placeholder="Enter your Email"
                                   name="email"
                                   value="{{$admin->email}}">
                            @error('email')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-lg-6">
                            <label>Roles :</label>
                            <select class="form-control kt-select2" id="kt_select2_3" name="roles_name[]"
                                    multiple="multiple">
                                @foreach ($roles as $role)
                                    <option
                                        {{in_array($role,$adminRole) ? 'selected' : false}} value="{{$role}}">{{$role}}</option>
                                @endforeach
                            </select>
                            @error('roles_name')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-lg-6">
                            <label>Gender :</label>
                            <select class="form-control kt-select2" id="kt_select2_10" name="gender">
                                <option></option>
                                <option @if ($admin->gender == 'Mael') selected @endif value="Mael">Mael</option>
                                <option @if ($admin->gender == 'Female') selected @endif value="Female">Female</option>
                            </select>
                            @error('gender')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-lg-6">
                            <label>Date Of Birth :</label>
                            <input type="text" class="form-control" id="kt_datepicker_1_modal" readonly
                                   placeholder="Select date" name="date_of_birth" value="{{$admin->date_of_birth}}"/>
                            @error('date_of_birth')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-lg-6">
                            <label>Marital Status :</label>
                            <select class="form-control kt-select2" id="kt_select2_10" name="marital_status">
                                <option></option>
                                <option @if ($admin->marital_status == 'Single') selected @endif value="Single">Single
                                </option>
                                <option @if ($admin->marital_status == 'married') selected @endif value="married">
                                    married
                                </option>
                            </select>
                            @error('marital_status')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-lg-6">
                            <label class="">Number Of Children :</label>
                            <input type="number" class="form-control" placeholder="Enter your Number Of Children"
                                   name="num_children"
                                   value="{{$admin->num_children}}">
                            @error('num_children')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-lg-6">
                            <label class="">Address :</label>
                            <input type="text" class="form-control" placeholder="Enter your Number Of Children"
                                   name="address"
                                   value="{{$admin->address}}">
                            @error('address')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-lg-6">
                            <label class="">Phone :</label>
                            <input type="number" class="form-control" placeholder="Enter your phone Number"
                                   name="phone"
                                   value="{{$admin->phone}}">
                            @error('phone')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-lg-6">
                            <label class="">Secondary Phone :</label>
                            <input type="number" class="form-control" placeholder="Enter your Secondary Phone Number"
                                   name="secondary_phone"
                                   value="{{$admin->secondary_phone}}">
                            @error('secondary_phone')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-lg-6">
                            <label class="">Telephone :</label>
                            <input type="number" class="form-control" placeholder="Enter your Telephone Number"
                                   name="telephone"
                                   value="{{$admin->telephone}}">
                            @error('telephone')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-lg-6">
                            <label class="">University :</label>
                            <input type="text" class="form-control" placeholder="Enter your University"
                                   name="university"
                                   value="{{$admin->university}}">
                            @error('university')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-lg-6">
                            <label class="">Faculty :</label>
                            <input type="text" class="form-control" placeholder="Enter your Faculty"
                                   name="faculty"
                                   value="{{$admin->faculty}}">
                            @error('faculty')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-lg-6">
                            <label class="">Specialization :</label>
                            <input type="text" class="form-control" placeholder="Enter your Specialization"
                                   name="specialization"
                                   value="{{$admin->specialization}}">
                            @error('specialization')
                            <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>

                    </div>

                    <div class="form-group row">
                        @foreach ($files as $file)
                            <div class="col-lg-6">
                                <label>{{$file->name}}:</label>
                                <div></div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="{{$file->name}}" id="customFile">
                                    <label class="custom-file-label" for="customFile">Choose File</label>
                                </div>
                                @error($file->name)
                                <span class="form-text text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        @endforeach
                    </div>

                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-lg-6">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <button type="reset" class="btn btn-secondary">Cancel</button>
                            </div>
                            <div class="col-lg-6 kt-align-right">
                                <button type="reset" class="btn btn-danger">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

        <!--end::Portlet-->
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/js/demo1/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script>
    <!--begin::Page Scripts(used by this page) -->
    <script src="{{asset('assets/js/demo1/pages/crud/forms/widgets/bootstrap-datepicker.js')}}"
            type="text/javascript"></script>
@endsection
