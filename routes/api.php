<?php

use App\Http\Controllers\API\Auth\LoginController;
use App\Http\Controllers\API\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('user/login',[LoginController::class,'login']);
Route::post('user/forget-password',[LoginController::class,'submitForgetPasswordForm']);

Route::group(['middleware' => 'auth:user-api'],function (){
    Route::post('user/logout',[LoginController::class,'logout']);
    Route::get('user/profile',[UserController::class,'profile']);
    Route::get('user/edit',[UserController::class,'edit']);
});


//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});
