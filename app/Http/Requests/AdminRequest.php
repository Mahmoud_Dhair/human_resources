<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$this->id,
//            'password' => 'required_without:id',
//            'phone' => 'nullable',
//            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
            'roles_name' => 'required',
        ];
    }

    public function messages()
    {
        return[
//            'name.required' => 'The Name Field Is Required',
            'email.required' => 'Email is required',
            'email.email' => 'Enter a valid email address',
            'roles_name.required' => 'User role is required',
//            'password.required_without' => 'The Password Field Is Required',
            'email.unique' => 'Email exist',
//            'image.image' => 'The File Must To Be Image',
//            'image.mimes' => 'The Format of Image Is (jpeg,png,jpg,gif,svg)',
        ];
    }
}
