<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $user = User::query()->create([
           'name_en'=>'superAdmin',
           'name_ar'=>'سوبر ادمن',
           'email'=>'admin@gmail.com',
           'password'=>Hash::make(123456789)
        ]);

        $user2 = User::query()->create([
            'name_en'=>'user',
            'name_ar'=>'مستخدم',
            'email'=>'user@gmail.com',
            'password'=>Hash::make(123456789)
        ]);
        $role2 = Role::create(['name' => 'user','guard_name' => 'web']);
        $role1 = Role::create(['name' => 'Admin','guard_name' => 'web']);
        $role = Role::create(['name' => 'SuperAdmin','guard_name' => 'web']);
        $permissions = Permission::query()->pluck('id','id')->all();
        $role->syncPermissions($permissions);
        $user->assignRole([$role->id,$role1->id]);
        $user2->assignRole([$role2->id]);
    }
}
